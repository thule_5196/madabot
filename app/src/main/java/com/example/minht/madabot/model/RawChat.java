package com.example.minht.madabot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minht on 12/11/2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder(builderMethodName = "builder")
public class RawChat {
    private String username;
    private String content;
    private String media;

}
