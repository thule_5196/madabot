package com.example.minht.madabot.adapters;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.minht.madabot.ChatListActivity;
import com.example.minht.madabot.IntroActivity;
import com.example.minht.madabot.R;

/**
 * Created by minht on 11/10/2017.
 */

public class IntroSwipeAdapter extends PagerAdapter {

    private int[] images_resources={R.drawable.intro1,R.drawable.intro2,R.drawable.intro3};

    private Context ctx;
    private LayoutInflater layoutInflater;

    public IntroSwipeAdapter(Context ctx){

        this.ctx=ctx;
    }
    @Override
    public int getCount() {
        return images_resources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==(RelativeLayout)object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater= (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view=layoutInflater.inflate(R.layout.swipe_layout,container,false);

        ImageView imageView = (ImageView) item_view.findViewById(R.id.intro_image_view);

        imageView.setImageResource(images_resources[position]);
        container.addView(item_view);

        Button skipButton= (Button) item_view.findViewById(R.id.skip_button);
        if (position >=2){
            skipButton.setVisibility(View.INVISIBLE);
            imageView.setClickable(false);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, ChatListActivity.class);
                    ctx.startActivity(intent);
                }
            });

        } else {
            skipButton.setVisibility(View.VISIBLE);
            imageView.setClickable(false);

        }

        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}
