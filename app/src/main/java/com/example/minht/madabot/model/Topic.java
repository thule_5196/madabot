package com.example.minht.madabot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minht on 11/11/2017.
 */


@Data
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder(builderMethodName = "builder")
public class Topic {

    private String id;
    private String name;
    private String description;
    private String filename;
}
