package com.example.minht.madabot;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.minht.madabot.model.Chat;
import com.example.minht.madabot.model.Topic;
import com.example.minht.madabot.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by minht on 12/9/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "contactsManager";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DES = "description";
    private static final String KEY_FILENAME = "filename";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE topic("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_DES + " TEXT, " + KEY_FILENAME+ " TEXT"+")";
        db.execSQL(CREATE_CONTACTS_TABLE);

        CREATE_CONTACTS_TABLE = "CREATE TABLE chat(id INTEGER PRIMARY KEY , line TEXT, froms TEXT, topic_id TEXT , date_sent TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS topic" );
        db.execSQL("DROP TABLE IF EXISTS chat" );

        // Create tables again
        onCreate(db);
    }



    Topic getTopic(int id){


        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("topic", new String[] { KEY_ID,
                        KEY_NAME, KEY_NAME,KEY_DES,KEY_FILENAME }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Topic topic=Topic.builder().id(cursor.getString(0)).name(cursor.getString(1)).description(cursor.getString(2)).filename(cursor.getString(3)).build();
        return topic;
    }
    public List<Topic> getAllTopic(){


        List<Topic> topicList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM topic";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Topic topic = new Topic();
                topic.setId(cursor.getString(0));
                topic.setName(cursor.getString(1));
                topic.setDescription(cursor.getString(2));
                topic.setFilename(cursor.getString(3));
                // Adding contact to list
                topicList.add(topic);
            } while (cursor.moveToNext());
        }

        // return contact list
        return topicList;
    }


    public int updateTopic(Topic topic) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, topic.getName());
        values.put(KEY_DES, topic.getDescription());
        values.put(KEY_FILENAME, topic.getFilename());

        // updating row
        return db.update("topic", values, KEY_ID + " = ?",
                new String[] { String.valueOf(topic.getId()) });
    }

    // Deleting single contact
    public void deleteTopic(Topic topic) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("topic", KEY_ID + " = ?",
                new String[] { String.valueOf(topic.getId()) });
        db.close();
    }

    public void deleteTopic(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("topic", KEY_ID + " = ?",
                new String[] { id });
        db.close();
    }
    public void deleteChats() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("chat","",null);
    }
    public void deleteChats(String topic_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete("chat","topic_id =?",new String[] { topic_id });
    }

    public  void addTopic(Topic topic) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, topic.getName());
        values.put(KEY_NAME, topic.getName());
        values.put(KEY_DES, topic.getDescription());
        values.put(KEY_FILENAME, topic.getFilename());
        // Inserting Row
        db.insert("topic", null, values);
        db.close(); // Closing database connection
    }


    public  void addChat(Chat chat) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("line", chat.getLine());
        values.put("froms", chat.getFrom());
        values.put("topic_id", chat.getTopic_id());
        values.put("date_sent", chat.getDateSent().toString());
        // Inserting Row
        db.insert("chat", null, values);
        db.close(); // Closing database connection
    }
    public List<Chat> getAllChatForId(String id){

        SQLiteDatabase db = this.getWritableDatabase();


        List<Chat> chatList = new ArrayList<>();
        // Select All Query



        String selectQuery = "SELECT  * FROM chat where topic_id =? ";
        Cursor cursor = db.rawQuery(selectQuery, new String[] {id});


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Chat c=Chat.builder().id(cursor.getString(0)).line(cursor.getString(1)).from(cursor.getString(2)).topic_id(cursor.getString(3)).dateSent(new Date(cursor.getString(4))).build();
                // Adding contact to list id,line,from,topic_id,date_sent
                chatList.add(c);
            } while (cursor.moveToNext());
        }

        // return contact list
        return chatList;
    }

    public List<Integer> getChatsLength(){

        SQLiteDatabase db = this.getWritableDatabase();


        List<Integer> chatList = new ArrayList<>();
        // Select All Query



        String selectQuery = "SELECT  count (*) FROM chat group by topic_id ";
        Cursor cursor = db.rawQuery(selectQuery,null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // Adding contact to list id,line,from,topic_id,date_sent
                chatList.add(Integer.parseInt(cursor.getString(0)));
            } while (cursor.moveToNext());
        }

        // return contact list
        return chatList;
    }


}
