package com.example.minht.madabot;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.minht.madabot.model.Chat;
import com.example.minht.madabot.model.Topic;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minht on 11/11/2017.
 */

public class ChatFragment extends Fragment {

    private static final String TAB1="TAB1";
    public  static List<Topic> chat_des=new ArrayList<>();
    private View view=null;
    CustomAdapter1 listViewAdapter;
    public static  ArrayList<Topic> chat_titles=new ArrayList<>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DatabaseHandler db = new DatabaseHandler(getActivity());
        chat_titles= (ArrayList<Topic>) db.getAllTopic();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        view =inflater.inflate(R.layout.chat_fragment,container,false);
        ListView listView = (ListView) view.findViewById(R.id.list);
        listViewAdapter = new CustomAdapter1(getContext());

        Log.d("color: ",listView.getBackground().toString()+"");
        listView.setAdapter(listViewAdapter);
        listViewAdapter.notifyDataSetChanged();

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    class CustomAdapter1 extends BaseAdapter{
        Context context;

        public CustomAdapter1(){

        }

        public CustomAdapter1(Context context){
            this.context=context;

        }
        @Override
        public int getCount() {
            return chat_titles.size();
        }

        @Override
        public Object getItem(int position) {
            return chat_titles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ChatFragment.CustomAdapter1.ViewHolder holder;
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = vi.inflate(R.layout.listview_row, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ChatFragment.CustomAdapter1.ViewHolder) convertView.getTag();
            }


            holder.title.setText(chat_titles.get(position).getName());
            holder.id=chat_titles.get(position).getId();
            holder.description.setText(chat_titles.get(position).getDescription());
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ChatConversationActivity.class);
                    String title= ((TextView) v).getText().toString();
                    intent.putExtra("title",title);
                    intent.putExtra("id",holder.id);
                    startActivity(intent);
                }
            });
            return convertView;
        }

        private ChatFragment.CustomAdapter1.ViewHolder createViewHolder(View v) {
            final ChatFragment.CustomAdapter1.ViewHolder holder = new ChatFragment.CustomAdapter1.ViewHolder();
            holder.title = (TextView) v.findViewById(R.id.chat_titles);
            holder.description=(TextView) v.findViewById(R.id.chat_des);
            holder.img=(ImageView) v.findViewById(R.id.chatimg);

            return holder;
        }

        public class ViewHolder {
            public TextView title;
            public TextView description;
            public ImageView img;
            public String id;
        }
    }




}
