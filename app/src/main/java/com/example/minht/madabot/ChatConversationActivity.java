package com.example.minht.madabot;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minht.madabot.model.Chat;
import com.example.minht.madabot.model.RawChat;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ChatConversationActivity extends AppCompatActivity {


    List<Chat> chats= new ArrayList<>();
    CustomAdapter adapter;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_conversation);

        Bundle extras = getIntent().getExtras();

        listView = (ListView) findViewById(R.id.conversation);

        adapter=new CustomAdapter(ChatConversationActivity.this);

        listView.setAdapter(adapter);
        listView.setSelection(listView.getAdapter().getCount()-1);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#012C40"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        if (extras!=null && extras.getString("title")!=null)
            getSupportActionBar().setTitle(extras.getString("title"));
        DatabaseHandler db = new DatabaseHandler(this);
        if (extras!=null && extras.getString("id")!=null){
            id=extras.getString("id");
            chats=db.getAllChatForId(id);
        }
    }
    String id;





    class CustomAdapter extends BaseAdapter{

        Activity context;

        public CustomAdapter(){

        }

        public CustomAdapter(Activity context){
            this.context=context;

        }

        @Override
        public int getCount() {
            return chats.size();
        }

        @Override
        public Object getItem(int position) {
            return chats.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final CustomAdapter.ViewHolder holder;
            Chat chatMessage = (Chat)getItem(position);
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = vi.inflate(R.layout.chat_row, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            boolean myMsg = chatMessage.getFrom().equals("me") ;//Just a dummy check
            Log.d(myMsg?"true ":"false","");
            setAlignment(holder, myMsg);
            holder.txtMessage.setText(""+chatMessage.getLine());


            DownloadImageTask dIT = new DownloadImageTask(holder.img);

            if (chatMessage.getMedia()!=null)
                dIT.execute(chatMessage.getMedia());

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ChatConversationActivity.this, ViewPictureActivity.class);
                    intent.putExtra("src",holder.img.getTag()+"");
                    startActivity(intent);
                }
            });
            return convertView;



        }
        private void setAlignment(ViewHolder holder, boolean isMe) {
            if (!isMe) {

                holder.txtMessage.setBackgroundColor(Color.parseColor("#DAEBF2"));
                holder.txtMessage.setTextColor(Color.parseColor("#FF404C"));

            } else {
                holder.contentWithBG.setGravity(Gravity.RIGHT);
                holder.contentWithBG.setGravity(Gravity.RIGHT);
                holder.txtMessage.setGravity(Gravity.RIGHT);


                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
                params.gravity = Gravity.RIGHT;
                holder.txtMessage.setLayoutParams(params);

                holder.txtMessage.setBackgroundColor(Color.parseColor("#1CA5B8"));
                holder.txtMessage.setTextColor(Color.parseColor("#FFFFFF"));

            }
        }

        private ViewHolder createViewHolder(View v) {
            final ViewHolder holder = new ViewHolder();
            holder.txtMessage = (TextView) v.findViewById(R.id.chatline_bot);
            holder.contentWithBG=(LinearLayout) v.findViewById(R.id.bubble_bot);
            holder.img=(ImageView) v.findViewById(R.id.chatimg);

            return holder;
        }

        public class ViewHolder {
            public TextView txtMessage;
            public LinearLayout contentWithBG;
            public ImageView img;
        }

    }


    public void send(View view){

        EditText chatField = (EditText)findViewById(R.id.chat_input);
        String chat=chatField.getText().toString();
        Chat temp;
        if (!chat.equals("")) {
            temp=Chat.builder().from("me").line(chat).topic_id(this.id).dateSent(new Date()).build();
            chats.add(temp);

            DatabaseHandler db = new DatabaseHandler(this);

            db.addChat(temp);
            chatField.setText("");
            Log.d("chat",chats.size()+"");

            final Gson gson = new Gson();
            final String topic_id=this.id;
            String json = gson.toJson(temp);

            /// here


            AsyncSendMessage runner = new AsyncSendMessage();
            runner.execute(json,topic_id);

            db.close();

        } else {

            Toast.makeText(this,"Empty Answer",Toast.LENGTH_SHORT);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_chat_list,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        DatabaseHandler db = new DatabaseHandler(this);

        int id=item.getItemId();
        if(id==R.id.action_clear){

            chats.clear();
            adapter.notifyDataSetChanged();
            db.deleteChats(this.id);
            return true;
        } else if (id==R.id.action_delete){

            db.deleteTopic(this.id);
            Intent intent = new Intent(this, ChatListActivity.class);
            startActivity(intent);
            return true;
        } else if(id==R.id.show_cheatsheet){

            Intent intent = new Intent(this, ViewPictureActivity.class);
            intent.putExtra("src",0+"");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    //    AsyncSendMessage runner = new AsyncSendMessage();
//    String sleepTime = time.getText().toString();
//                runner.execute(sleepTime);




    private class AsyncSendMessage extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(final String... params) {

            SyncHttpClient client = new SyncHttpClient( );

            try {
                client.post(null, "http://192.168.43.221:8080/message/", new StringEntity(params[0]), "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                final Gson gson = new Gson();

                                RawChat r=gson.fromJson(new String(responseBody),RawChat.class);
                                Chat c=Chat.builder().from("bot").line(r.getContent()).dateSent(new Date()).media(r.getMedia()).topic_id(params[1]).build();
                                DatabaseHandler db = new DatabaseHandler(ChatConversationActivity.this);

                                db.addChat(c);
                                chats.add(c);
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
// you will never hear back from the thing, just send more in

                            }
                        });
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }





            return null;
        }







    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(final Bitmap result) {
            bmImage.setImageBitmap(result);
            bmImage.setTag(R.drawable.logo);
            bmImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ChatConversationActivity.this, ViewPictureActivity.class);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    result.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    intent.putExtra("bitmap",byteArray);
                    startActivity(intent);
                }
            });
        }
    }






}










