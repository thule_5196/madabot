package com.example.minht.madabot;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.example.minht.madabot.adapters.SectionPageAdapter;
import com.example.minht.madabot.model.Topic;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.app.PendingIntent.getActivity;
import static com.example.minht.madabot.R.drawable.night;
import static com.example.minht.madabot.R.drawable.sunny;

public class ChatListActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private SectionPageAdapter mSectionPageAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        mSectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        mViewPager =(ViewPager) findViewById(R.id.container);
        init(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        onButtonClicked();

        LinearLayout linearLayout=(LinearLayout) findViewById(R.id.profile);
        Calendar c=Calendar.getInstance();
        if(c.get(Calendar.HOUR_OF_DAY)>18){
                linearLayout.setBackgroundColor(Color.parseColor("#1CA5B8"));

        } else{
            linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));

        }

        ((ImageView) findViewById(R.id.prof)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Uri gmmIntentUri = Uri.parse("geo:0,0?q=school");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps"); // build in google map
                        startActivity(mapIntent);
                    }
                }, 1000);
            }
        });




    }

    public void init(ViewPager viewPager){

        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new StatsFragment(),"Stats");
        adapter.addFragment(new ChatFragment(),"Chat");
        adapter.addFragment(new CheatsheetFragment(),"Cheat Sheet");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new ChatFragment(), "fragment_tag").commit();    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }



    String editText;
    Spinner difficulty;
    private FloatingActionButton add_button;
    private void onButtonClicked(){
        add_button=(FloatingActionButton) findViewById(R.id.fab);
        add_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ChatListActivity.this);

                LayoutInflater inflater = getLayoutInflater();

                builder.setView(inflater.inflate(R.layout.new_chat_fragment, null))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Dialog dialogView = (Dialog) dialog;

                                EditText topicName= (EditText) dialogView.findViewById(R.id.new_chat_name) ;
                                SeekBar description= (SeekBar) dialogView.findViewById(R.id.diff) ;
                                String descriptionStr = description.getProgress()+"";
                                String topicNameStr=topicName.getText().toString();
                                DatabaseHandler db = new DatabaseHandler(ChatListActivity.this);

                                db.addTopic(Topic.builder().name(topicNameStr).description("Difficulty: "+descriptionStr).build());

                                Intent intent = new Intent(ChatListActivity.this, ChatConversationActivity.class);
                                intent.putExtra("title",topicNameStr+": "+descriptionStr);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });




                android.app.AlertDialog alert=builder.create();
                alert.show();
            }
        });
    }






}
