package com.example.minht.madabot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.minht.madabot.model.User;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText usernameField = (EditText)findViewById(R.id.usernameInput);
        EditText passwordField = (EditText)findViewById(R.id.passwordInput);

        usernameField.setSelectAllOnFocus(true);
        passwordField.setSelectAllOnFocus(true);


    }

    public void doSignin(View v) throws UnsupportedEncodingException {

        EditText usernameField = (EditText)findViewById(R.id.usernameInput);
        EditText passwordField = (EditText)findViewById(R.id.passwordInput);


        String username=usernameField.getText().toString();
        String password=passwordField.getText().toString();

        if (username.equals("")||password.equals("")){
            Toast.makeText(this, "Enter your username and password", Toast.LENGTH_SHORT).show();

        } else {
            AsyncHttpClient client = new AsyncHttpClient();
            User u = new User(username,password);
            Gson gson = new Gson();
            String json = gson.toJson(u);
            client.post(null, "http://192.168.43.221:8080/login/", new StringEntity(json), "application/json",
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                            String temp=new String(responseBody);
                                if (temp.equals("usser logged in")){
                                Intent intent = new Intent(LoginActivity.this, IntroActivity.class);
                                startActivity(intent);

                                Toast.makeText(LoginActivity.this, "logged in" + temp, Toast.LENGTH_SHORT).show();
                                LoginActivity.this.finish();}
                                else {

                                    LoginActivity.this.findViewById(R.id.errMessage).setVisibility(View.VISIBLE);
                                }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }
                    });




        }



    }
}
