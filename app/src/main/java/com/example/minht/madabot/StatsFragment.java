package com.example.minht.madabot;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.*;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

/**
 * Created by minht on 11/11/2017.
 */

public class StatsFragment extends android.support.v4.app.Fragment {
    private static final String TAB1="TAB1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view =inflater.inflate(R.layout.stats_fragment,container,false);
        DatabaseHandler db = new DatabaseHandler(getActivity());

        DataPoint[] dP= new DataPoint[db.getChatsLength().size()];

        for(int i=0;i<db.getChatsLength().size();i++){

            dP[i] = new DataPoint((double) i,(double) (db.getChatsLength().get(i)));
        }
        GraphView graph = (GraphView) view.findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dP);
        graph.setBackgroundColor(Color.WHITE);
        graph.setTitle("Number of Correct Answer By Days");
        graph.addSeries(series);

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(new String[] {"Day 1", "Day 2", "Day 3", "Day 4"});
        staticLabelsFormatter.setVerticalLabels(new String[] {"10", "20", "30"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(true);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(true);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.BLACK);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.BLACK);


        GraphView graph1 = (GraphView) view.findViewById(R.id.graph1);

        BarGraphSeries<DataPoint> series1 = new BarGraphSeries<>(dP);

        graph1.setBackgroundColor(Color.WHITE);
        graph1.setTitleColor(Color.parseColor("#E21738"));

        graph1.setTitle("Chat Length");

        graph1.addSeries(series1);

        staticLabelsFormatter.setHorizontalLabels(new String[] {"Day 1", "Day 2", "Day 3", "Day 4"});
        staticLabelsFormatter.setVerticalLabels(new String[] {"10", "20", "30"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(true);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(true);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.BLACK);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.BLACK);


        GraphView graph3 = (GraphView) view.findViewById(R.id.graph3);
        LineGraphSeries<DataPoint> series3 = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 1),
                new DataPoint(1, 5),
                new DataPoint(2, 3),
                new DataPoint(3, 2),
                new DataPoint(4, 6)
        });
        graph3.setBackgroundColor(Color.WHITE);
        graph3.setTitle("Number of Correct Answer By Days");
        graph3.addSeries(series3);


        StaticLabelsFormatter staticLabelsFormatter1 = new StaticLabelsFormatter(graph);

        graph3.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter1);
        graph3.getGridLabelRenderer().setVerticalLabelsVisible(true);
        graph3.getGridLabelRenderer().setHorizontalLabelsVisible(true);
        graph3.getGridLabelRenderer().setVerticalLabelsColor(Color.BLACK);
        graph3.getGridLabelRenderer().setHorizontalLabelsColor(Color.BLACK);
        return view;

    }
}
