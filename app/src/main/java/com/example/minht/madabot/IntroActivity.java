package com.example.minht.madabot;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.minht.madabot.adapters.IntroSwipeAdapter;

public class IntroActivity extends AppCompatActivity {

    ViewPager viewPager;
    IntroSwipeAdapter introSwipeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        viewPager= (ViewPager) findViewById(R.id.intro_galery);
        introSwipeAdapter = new IntroSwipeAdapter(this);
        viewPager.setAdapter(introSwipeAdapter);

        Button skip_button = (Button)findViewById(R.id.skip_button);


    }



    public void toMainScreen(View view){
        Toast.makeText(this,"ok "+viewPager.getCurrentItem(),Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ChatListActivity.class);
        startActivity(intent);
        finish();
    }
}
